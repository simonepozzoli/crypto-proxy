import argparse
import asyncio
import logging
import os
import sys
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes

# Parse arguments
parser = argparse.ArgumentParser(prog='crypto-proxy')
parser.add_argument('secret-key', help='Path to 256-bit secret key file.')
parser.add_argument('-lp', '--port', default='5000', type=int, help='Local proxy port.')
parser.add_argument('-rip', '--remote-ip', default='localhost', help='Remote proxy IP.')
parser.add_argument('-rp', '--remote-port', default='7000', type=int, help='Remote proxy port.')
parser.add_argument('-ar', '--allow-remote', action='store_true',
                    help='Allow remote connections to local proxy.')
parser.add_argument('-cs', '--client-server', default='client', choices=['client', 'server'],
                    help='Run the proxy in client or server mode.')

arguments = parser.parse_args(sys.argv[1:])

local_port = str(arguments.port)
secret_key_path = getattr(arguments, 'secret-key')
allow_remote = arguments.allow_remote
client_server = arguments.client_server
remote_host = arguments.remote_ip
remote_port = str(arguments.remote_port)
destination_address = ':'.join((remote_host, remote_port))

# Setup logging
logger = logging.getLogger(__name__)
formatter = logging.Formatter('[%(asctime)s] %(l_address)-21s %(direction)s {r_address} - %(message)s'.format(
    r_address=destination_address
))
handler = logging.StreamHandler(sys.stdout)
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)

print("""

Proxy configuration:
 Running as {client_server}.
  - port: {port}
  - allow_remote: {allow_remote}

""".format(
    client_server=client_server, port=local_port, allow_remote=allow_remote
))


CHUNK_SIZE = 256


# Read secret key and prepare cipher
with open(secret_key_path, 'rb') as f:
    key = f.read()
cypher_algorithm = algorithms.AES(key)
cypher_algorithm.block_size = CHUNK_SIZE


def _encrypt(data):
    iv = os.urandom(32)
    cipher = Cipher(cypher_algorithm, modes.CBC(iv))
    encryptor = cipher.encryptor()
    data = encryptor.update(data)
    data += encryptor.finalize()
    return iv + data


def _decrypt(data):
    iv, data = data[:32], data[32:]
    cipher = Cipher(cypher_algorithm, modes.CBC(iv))
    decryptor = cipher.decryptor()
    data = decryptor.update(data)
    data += decryptor.finalize()
    return data


def _pad(data):
    length = CHUNK_SIZE - len(data)
    data += bytes([length]) * length
    return data


def _unpad(data):
    return data[:-data[-1]]


async def proxy(reader, writer, direction, source_address):
    while True:
        if direction:
            try:
                data = await reader.read(CHUNK_SIZE - 1)
            except ConnectionError:
                break
            if not data:
                break
            # Add padding
            data = _pad(data)
            # Encrypt data before sending stream
            data = _encrypt(data)

        else:
            try:
                data = await reader.readexactly(CHUNK_SIZE + 32)
            except (ConnectionError, asyncio.IncompleteReadError):
                break
            if not data:
                break
            # Decrypt incoming stream
            data = _decrypt(data)
            # Remove padding
            data = _unpad(data)

        try:
            writer.write(data)
            await writer.drain()
        except ConnectionError:
            break

    writer.close()
    await writer.wait_closed()
    logger.info(
        'Closed connection',
        extra={
            'l_address': source_address,
            'direction': '>X<'
        }
    )


async def handle_connection(source_reader, source_writer):
    target_reader, target_writer = await asyncio.open_connection(remote_host, remote_port)
    source_ip = source_writer.get_extra_info('peername')[0]
    source_port = source_writer.get_extra_info('peername')[1]
    source_address = '{}:{}'.format(
        source_ip,
        source_port
    )
    extra = {
        'l_address': source_address,
        'direction': '<&>'
    }
    logger.info('Created connection', extra=extra)
    if client_server == 'client':
        await asyncio.wait([
            proxy(source_reader, target_writer, True, source_address),
            proxy(target_reader, source_writer, False, source_address)
        ])
    else:
        await asyncio.wait([
            proxy(source_reader, target_writer, False, source_address),
            proxy(target_reader, source_writer, True, source_address)
        ])


loop = asyncio.get_event_loop()
if allow_remote:
    coro = asyncio.start_server(handle_connection, '', local_port, loop=loop)
else:
    coro = asyncio.start_server(handle_connection, '127.0.0.1', local_port, loop=loop)
server = loop.run_until_complete(coro)

logger.info(
    'Started proxy',
    extra={'l_address': 'localhost:{}'.format(local_port), 'direction': '-->'}
)

try:
    loop.run_forever()
except KeyboardInterrupt:
    pass

server.close()
loop.run_until_complete(server.wait_closed())
loop.close()
logger.info(
    'Stopped proxy',
    extra={'l_address': 'localhost:{}'.format(local_port), 'direction': '-/>'}
)
