altgraph==0.17
cffi==1.14.5
cryptography==3.4.7
pycparser==2.20
pyinstaller==4.2
pyinstaller-hooks-contrib==2021.1
