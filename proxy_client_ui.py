import asyncio
import os
import webbrowser

import tkinter as tk
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from tkinter import filedialog


HELP_TEXT = """----- PrivateRadio Client instructions -----

To start the radio follow these steps:

  1) Select the secret key.
  2) Enter local proxy port (optional, default=5000)
  3) Press "Start Proxy"

A browser page will open at the radio homepage, you can listen to whatever is playing.

If you want to start a stream, follow a guide on how to stream to icecast2, any of the
tens of existing clients should be ok.
The password to upload the stream (source password) is "hackme"."""

# Configuring UI
window = tk.Tk()
window.title('PrivateRadio')

help_frame = tk.Frame()
help_frame.pack()
local_proxy_port_frame = tk.Frame()
local_proxy_port_frame.pack()
proxy_control_frame = tk.Frame()
proxy_control_frame.pack()

local_proxy_port_label = tk.Label(master=local_proxy_port_frame, text='Local proxy port:')
local_proxy_port_label.pack(side=tk.LEFT)
local_proxy_port_entry = tk.Entry(master=local_proxy_port_frame, width=5)
local_proxy_port_entry.insert(0, '5000')
local_proxy_port_entry.pack(side=tk.RIGHT)


def get_help():
    help_window = tk.Toplevel(window)
    # sets the title of the
    # Toplevel widget
    help_window.title("Help")
    text = tk.Label(help_window, text=HELP_TEXT, height=13, width=70)
    text.pack()


help_button = tk.Button(master=help_frame, text='?', command=get_help)
help_button.pack(side=tk.LEFT)


def upload_key(event=None):
    global key, secret_key_label
    secret_key_path = filedialog.askopenfilename()
    if secret_key_path:
        try:
            with open(secret_key_path, 'rb') as f:
                key = f.read()
        except Exception as e:
            print(e)
        else:
            secret_key_label.config(text='OK')


key = b''

secret_key_label = tk.Label(master=help_frame, text='')
secret_key_label.pack(side=tk.RIGHT)

secret_key_button = tk.Button(master=help_frame, text='Secret key', command=upload_key)
secret_key_button.pack(side=tk.RIGHT)


def start_proxy():
    global key
    local_port = local_proxy_port_entry.get()
    if not (local_port and key):
        return
    loop, server = prepare_proxy(local_port)
    webbrowser.open('http://localhost:{}'.format(local_port))
    window.destroy()
    run_proxy(loop, server)


allow_remote_connections = tk.BooleanVar()
allow_remote_select = tk.Checkbutton(
    master=proxy_control_frame,
    variable=allow_remote_connections,
    text='Allow remote connections'
)
allow_remote_select.pack()

start_proxy_button = tk.Button(master=proxy_control_frame, text='Start proxy', command=start_proxy)
start_proxy_button.pack()

# Setting remote address
remote_host = 'songspotting.ddns.net'
remote_port = '7000'
destination_address = ':'.join((remote_host, remote_port))


async def proxy(reader, writer, direction):
    global key
    while True:
        if direction:
            # Read at most 255 bytes, at least one
            # will be added as padding so that the
            # total packet length is 256
            data = await reader.read(255)
            if not data:
                break
            # Add padding
            length = 255 - len(data)
            data += bytes([length]) * (length + 1)
            # Encrypt data before sending stream
            iv = os.urandom(16)
            cipher = Cipher(algorithms.AES(key), modes.CBC(iv))
            encryptor = cipher.encryptor()
            data = encryptor.update(data)
            data += encryptor.finalize()
            data = iv + data
        else:
            try:
                # We need exactly 256 bytes to decrypt,
                # this is the size of the packet that
                # was encrypted
                data = await reader.readexactly(272)
            except asyncio.IncompleteReadError:
                break
            # Decrypt incoming stream
            iv, data = data[:16], data[16:]
            cipher = Cipher(algorithms.AES(key), modes.CBC(iv))
            decryptor = cipher.decryptor()
            data = decryptor.update(data)
            data += decryptor.finalize()
            # Remove padding
            data = data[:-(data[-1]+1)]

        writer.write(data)
        await writer.drain()

    writer.close()


async def handle_connection(source_reader, source_writer):
    target_reader, target_writer = await asyncio.open_connection(remote_host, remote_port)
    await asyncio.wait([
        proxy(source_reader, target_writer, True),
        proxy(target_reader, source_writer, False)
    ])


def prepare_proxy(local_port):
    loop = asyncio.get_event_loop()
    if allow_remote_connections.get():
        coro = asyncio.start_server(handle_connection, '', local_port, loop=loop)
    else:
        coro = asyncio.start_server(
            handle_connection, '127.0.0.1', local_port, loop=loop
        )
    server = loop.run_until_complete(coro)
    return loop, server


def run_proxy(loop, server):
    print('\nPress CTRL-C or close the window to exit.\n')

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass

    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()


window.mainloop()
